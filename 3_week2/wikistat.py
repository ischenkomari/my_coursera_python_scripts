from bs4 import BeautifulSoup
import re
import os


# Вспомогательная функция, её наличие не обязательно и не будет проверяться
def build_tree(start, end, path):
    files = dict.fromkeys(os.listdir(path))  # Словарь вида {"filename1": None, "filename2": None, ...}
    files_set = set(files.keys())
    current_file = start
    previous_file_list = set()
    file_list = set()
    while True:
        with open("{}{}".format(path, current_file), 'r', encoding='utf8', errors='ignore') as data:
            soup = BeautifulSoup(data, "lxml")
            links = [link['href'].strip("/wiki/") for link in soup.body.find('div', attrs={'id': 'bodyContent'}).find_all('a', href=True) if re.match('^/wiki/[a-zA-Z0-9_()]+$', link['href'])]
        values = list(files_set & set(links))
        if current_file in values:
            values.remove(current_file)
        files[current_file] = values
        if end in values:
            break

        file_list.update(values)
        if not previous_file_list:
            previous_file_list = file_list
            file_list = set()
        for file in previous_file_list:
            if files[file] == None:
                current_file = file
                previous_file_list.remove(file)
                break

    files = {k: v for k, v in files.items() if v is not None}
    return files


# Вспомогательная функция, её наличие не обязательно и не будет проверяться
def build_bridge(start, end, path):
    files = build_tree(start, end, path)
    bridge = [end]
    items = list(files[start])

    while start not in bridge:
        new_items = set()
        if end in items:
            bridge.append(start)
            break
        for i in items:
            if i in files.keys():
                if end in files[i]:
                    bridge.append(i)
                    end = i
                    new_items = set(files[start])
                    break
                else:
                    new_items.update(files[i])
        items = list(new_items)
    return bridge


def parse(start, end, path):
    """
    Если не получается найти список страниц bridge, через ссылки на которых можно добраться от start до end, то,
    по крайней мере, известны сами start и end, и можно распарсить хотя бы их: bridge = [end, start]. Оценка за тест,
    в этом случае, будет сильно снижена, но на минимальный проходной балл наберется, и тест будет пройден.
    Чтобы получить максимальный балл, придется искать все страницы. Удачи!
    """

    bridge = build_bridge(start, end, path)  # Искать список страниц можно как угодно, даже так: bridge = [end, start]

    # Когда есть список страниц, из них нужно вытащить данные и вернуть их
    out = {}
    for file in bridge:
        with open("{}{}".format(path, file), 'r', encoding='utf8', errors='ignore') as data:
            soup = BeautifulSoup(data, "lxml")

        body = soup.find(id="bodyContent")

        # TODO посчитать реальные значения
        # Количество картинок (img) с шириной (width) не меньше 200
        imgs = len([link for link in body.find_all('img', width=True) if int(link['width'])>=200])
        # Количество заголовков, первая буква текста внутри которого: E, T или C
        headers = len([link.text for link in body.find_all(name=re.compile('^h')) if re.match('^E|T|C', link.text)])
        # Длина максимальной последовательности ссылок, между которыми нет других тегов
        tag = body.find_next("a")
        linkslen = -1
        while tag:
            curlen = 1
            for tag in tag.find_next_siblings():
                if tag.name != 'a':
                    break
                curlen += 1
            if curlen > linkslen:
                linkslen = curlen
            tag = tag.find_next("a")
        # Количество списков, не вложенных в другие списки
        lists = len([link.parent.name for link in body.find_all(name=re.compile('^[u|o]l')) if link.parent.name not in ['li']])

        out[file] = [imgs, headers, linkslen, lists]

    return out
