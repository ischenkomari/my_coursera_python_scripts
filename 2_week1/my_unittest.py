"""
Написать комплект тестов:

    test_wrong_types_raise_exception
    test_negative
    test_zero_and_one_cases
    test_simple_numbers
    test_two_simple_multipliers
    test_many_multipliers

Проверить в них соответственно:

    Что типы float и str (значения 'string', 1.5) вызывают исключение TypeError.
    Что для отрицательных чисел -1, -10 и -100 вызывается исключение ValueError.
    Что для числа 0 возвращается кортеж (0,), а для числа 1 кортеж (1,)
    Что для простых чисел 3, 13, 29 возвращается кортеж, содержащий одно данное число.
    Что для чисел 6, 26, 121 возвращаются соответственно кортежи (2, 3), (2, 13) и (11, 11).
    Что для чисел 1001 и 9699690 возвращаются соответственно кортежи (7, 11, 13) и (2, 3, 5, 7, 11, 13, 17, 19).

При этом несколько различных проверок в рамках одного теста должны быть обработаны как подслучаи с указанием x: subTest(x=...).

ВАЖНО! Название переменной в тестовом случае должно быть именно "x". Все входные данные должны быть такими, как указано в условии. В задании необходимо реализовать ТОЛЬКО класс TestFactorize, кроме этого реализовывать ничего не нужно. Импортировать unittest и вызывать unittest.main() в решении также не нужно.
"""
import unittest


class TestFactorize(unittest.TestCase):
    def test_wrong_types_raise_exception(self):
        """Что типы float и str (значения 'string', 1.5) вызывают исключение TypeError"""
        self.cases = ('string', 1.5)
        for i in self.cases:
            with self.subTest(case=1):
                self.assertRaises(TypeError, factorize, i)

    def test_negative(self):
        """Что для отрицательных чисел -1, -10 и -100 вызывается исключение ValueError"""
        self.cases = (-1, -10, -100)
        for i in self.cases:
            with self.subTest(case=i):
                self.assertRaises(ValueError, factorize, i)

    def test_zero_and_one_cases(self):
        """Что для числа 0 возвращается кортеж (0,), а для числа 1 кортеж (1,)"""
        self.cases = (0, 1)
        for i in self.cases:
            with self.subTest(case=i):
                self.assertTupleEqual(factorize(i), (i, ))

    def test_simple_numbers(self):
        """Что для простых чисел 3, 13, 29 возвращается кортеж, содержащий одно данное число"""
        self.cases = (3, 13, 29)
        for i in self.cases:
            with self.subTest(case=i):
               self.assertTupleEqual(factorize(i), (i, ))

    def test_two_simple_multipliers(self):
        """Что для чисел 6, 26, 121 возвращаются соответственно кортежи (2, 3), (2, 13) и (11, 11)"""
        self.cases = {6: (2, 3),
                      26: (2, 13),
                      121: (11, 11)}
        for k, v in self.cases.items():
            with self.subTest(case=k):
                self.assertTupleEqual(factorize(k), v)

    def test_many_multipliers(self):
        """Что для чисел 1001 и 9699690 возвращаются соответственно кортежи (7, 11, 13) и (2, 3, 5, 7, 11, 13, 17, 19)"""
        self.cases = {1001: (7, 11, 13),
                      9699690: (2, 3, 5, 7, 11, 13, 17, 19)}
        for k, v in self.cases.items():
            with self.subTest(case=k):
                self.assertTupleEqual(factorize(k), v)


def factorize(x):
    """ Factorize positive integer and return its factors.
        :type x: int,>=0
        :rtype: tuple[N],N>0
    """
    if not isinstance(x, int):
        raise TypeError
    if not x >= 0:
        raise ValueError
    result = simple_deviders(x)
    multiplied_list = list_multiply(result)
    while x > multiplied_list:
        div_result = int(x / multiplied_list)
        sd = simple_deviders(div_result)
        if len(sd) is 1:
            result = sd + result
            multiplied_list = multiplied_list * sd[0]
    return result


def all_dividers(a):
    output = tuple()
    if (a is 0) or (a is 1):
        output = (a,)
    else:
        for i in range(2, a+1):
            if a % i == 0:
                output = output + (i,)
    return output


def simple_deviders(a):
    result = tuple()
    for i in all_dividers(a):
        if len(all_dividers(i)) is 1:
            result = result + (i,)
    return result


def list_multiply(a):
    result = 1
    for i in a:
        result = result * i
    return result


if __name__ == "__main__":
    unittest.main()
