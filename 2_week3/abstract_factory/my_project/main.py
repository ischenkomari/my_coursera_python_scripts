from easylevel import *
from hardlevel import *
from mediumlevel import *


def create_level_map(factory):
    map = factory.get_map()

    object = factory.get_objects(map)

    return object


easy_map = create_level_map(EasyLevel)
print(easy_map)
