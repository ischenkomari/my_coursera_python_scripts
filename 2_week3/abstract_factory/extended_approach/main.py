import yaml
from assassin import *
from mage import *
from warrior import *


# player = create_hero(WarriorFactory())
# player.hit()
# player.cast()

def factory_constructor(loader, node):
    data = loader.construct_scalar(node)
    if data == "assassin":
        return AssassinFactory
    if data == "mage":
        return MageFactory
    else:
        return WarriorFactory


class Character(yaml.YAMLObject):
    yaml_tag = "!Character"

    def create_hero(self):
        hero = self.factory.create_hero(self.name)

        weapon = self.factory.create_weapon()
        spell = self.factory.create_spell()

        hero.add_weapon(weapon)
        hero.add_spell(spell)

        return hero


hero_yaml = '''
--- !Character
factory:
    !factory assassin
name:
    SomeName
'''

print(yaml.__file__)

loader = yaml.Loader
loader.add_constructor("!factory", factory_constructor)
hero = yaml.load(hero_yaml).create_hero()

hero.hit()
hero.cast()
