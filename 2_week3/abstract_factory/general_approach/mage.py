from hero import *


class MageFactory(HeroFactory):
    def create_hero(self, name):
        return Mage(name)

    def create_spell(self):
        return Fireball()

    def create_weapon(self):
        return Staff()


class Mage:
    def __init__(self, name):
        self.name = name
        self.spell = None
        self.armor = None
        self.weapon = None

    def add_weapon(self, weapon):
        self.weapon = weapon

    def add_spell(self, spell):
        self.spell = spell

    def hit(self):
        print(f"Mage {self.name} hits with {self.weapon.hit()}")

    def cast(self):
        print(f"Mage {self.name} casts {self.spell.cast()}")


class Staff:
    def hit(self):
        return "Staff"


class Fireball:
    def cast(self):
        return "Fireball"
