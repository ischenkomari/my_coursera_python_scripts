from hero import *


class AssassinFactory(HeroFactory):
    def create_hero(self, name):
        return Assassin(name)

    def create_spell(self):
        return Invisible()

    def create_weapon(self):
        return Dagger()


class Assassin:
    def __init__(self, name):
        self.name = name
        self.spell = None
        self.armor = None
        self.weapon = None

    def add_weapon(self, weapon):
        self.weapon = weapon

    def add_spell(self, spell):
        self.spell = spell

    def hit(self):
        print(f"Assassin {self.name} hits with {self.weapon.hit()}")

    def cast(self):
        print(f"Assassin {self.name} casts {self.spell.cast()}")


class Dagger:
    def hit(self):
        return "Dagger"


class Invisible:
    def cast(self):
        return "Invisibility"
