import yaml
from abstractlevel import *
from easylevel import *
from hardlevel import *
from mediumlevel import *


def easy_map_constructor(loader, node):
    _map = EasyLevel.Map()
    _obj = EasyLevel.Objects()
    return {'map': _map, 'obj': _obj}


def medium_map_constructor(loader, node):
    _map = MediumLevel.Map()
    _obj = MediumLevel.Objects()
    _obj.config = loader.construct_mapping(node)
    return {'map': _map, 'obj': _obj}


def hard_map_constructor(loader, node):
    _map = HardLevel.Map()
    _obj = HardLevel.Objects()
    _obj.config = loader.construct_mapping(node)
    return {'map': _map, 'obj': _obj}


loader = yaml.Loader
loader.add_constructor("!easy_level", easy_map_constructor)
loader.add_constructor("!medium_level", medium_map_constructor)
loader.add_constructor("!hard_level", hard_map_constructor)

Lebels = yaml.load('''
levels:
  - !easy_level {}
  - !medium_level
    enemy: ['rat']
  - !hard_level
    enemy: 
    - 'rat'
    - 'snake'
    - 'dragon'
    enemy_count: 10''')

Levels = {'levels': []}
_map = EasyLevel.Map()
_obj = EasyLevel.Objects()
Levels['levels'].append({'map': _map, 'obj': _obj})

_map = MediumLevel.Map()
_obj = MediumLevel.Objects()
_obj.config = {'enemy': ['rat']}
Levels['levels'].append({'map': _map, 'obj': _obj})

_map = HardLevel.Map()
_obj = HardLevel.Objects()
_obj.config = {'enemy': ['rat', 'snake', 'dragon'], 'enemy_count': 10}
Levels['levels'].append({'map': _map, 'obj': _obj})

print(Levels)
print(Lebels)
