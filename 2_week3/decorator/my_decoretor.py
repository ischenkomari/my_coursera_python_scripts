from abc import ABC, abstractmethod


class Creature(ABC):

    @abstractmethod
    def feed(self):
        pass

    @abstractmethod
    def move(self):
        pass

    @abstractmethod
    def make_noise(self):
        pass


class Animal(Creature):

    def feed(self):
        print("I eat grass")

    def move(self):
        print("I walk forward")

    def make_noise(self):
        print("WOOO!")


class AbstractDecorator(Creature):

    def __init__(self, base):
        self.base = base

    def feed(self):
        self.base.feed()

    def move(self):
        self.base.move()

    def make_noise(self):
        self.base.make_noise()


class Swimming(AbstractDecorator):

    def move(self):
        print("I swim forward")

    def make_noise(self):
        print("...")


class Predator(AbstractDecorator):

    def feed(self):
        print("I eat other animals")


class Fast(AbstractDecorator):

    def move(self):
        self.base.move()
        print("Fast!")


animal = Animal()

animal.feed()
animal.move()
animal.make_noise()

swimming = Swimming(animal)

swimming.feed()
swimming.move()
swimming.make_noise()

predator = Predator(swimming)

predator.feed()
predator.move()
predator.make_noise()

fast = Fast(predator)

fast.feed()
fast.move()
fast.make_noise()

faster = Fast(fast)

faster.feed()
faster.move()
faster.make_noise()

# убираем хищника
faster.base.base = faster.base.base.base
