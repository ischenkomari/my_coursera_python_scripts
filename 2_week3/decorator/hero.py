from abc import ABC, abstractmethod


class Hero:
    def __init__(self):
        self.positive_effects = []
        self.negative_effects = []

        self.stats = {
            "HP": 128,
            "MP": 42,
            "SP": 100,

            "Strength": 15,
            "Perception": 4,
            "Endurance": 8,
            "Charisma": 2,
            "Intelligence": 3,
            "Agility": 8,
            "Luck": 1
        }

    def get_positive_effects(self):
        return self.positive_effects.copy()

    def get_negative_effects(self):
        return self.negative_effects.copy()

    def get_stats(self):
        return self.stats.copy()


class AbstractEffect(Hero, ABC):
    def __init__(self, base):
        self.base = base

    def get_stats(self):
        """ Возвращает итоговые характеристики после применения эффекта"""
        return self.base.stats

    def get_positive_effects(self):
        return self.base.get_positive_effects()

    def get_negative_effects(self):
        return self.base.get_negative_effects()


class AbstractPositive(AbstractEffect):
    @abstractmethod
    def get_stats(self):
        pass

    @abstractmethod
    def get_positive_effects(self):
        pass


class AbstractNegative(AbstractEffect):
    @abstractmethod
    def get_stats(self):
        pass

    @abstractmethod
    def get_negative_effects(self):
        pass


class Bersek(AbstractPositive):
    """Увеличивает параметры Сила, Выносливость, Ловкость, Удача на 7; уменьшает параметры Восприятие, Харизма, Интеллект на 3. Количество единиц здоровья увеличивается на 50"""

    def __init__(self, base):
        self.base = base

        self.stats = self.base.get_stats()

        self.stats["HP"] += 50
        self.stats["Strength"] += 7
        self.stats["Perception"] -= 3
        self.stats["Endurance"] += 7
        self.stats["Charisma"] -= 3
        self.stats["Intelligence"] -= 3
        self.stats["Agility"] += 7
        self.stats["Luck"] += 7

        self.positive_effects = self.base.get_positive_effects()

        self.positive_effects.append(__class__.__name__)

    def get_stats(self):
        return self.stats

    def get_positive_effects(self):
        return self.positive_effects


class Blessing(AbstractPositive):
    """Увеличивает все основные характеристики на 2"""

    def __init__(self, base):
        self.base = base

        self.stats = self.base.get_stats()

        self.stats["HP"] += 2
        self.stats["MP"] += 2
        self.stats["SP"] += 2
        self.stats["Strength"] += 2
        self.stats["Perception"] += 2
        self.stats["Endurance"] += 2
        self.stats["Charisma"] += 2
        self.stats["Intelligence"] += 2
        self.stats["Agility"] += 2
        self.stats["Luck"] += 2

        self.positive_effects = self.base.get_positive_effects()

        self.positive_effects.append(__class__.__name__)

    def get_stats(self):
        return self.stats

    def get_positive_effects(self):
        return self.positive_effects


class Weakness(AbstractNegative):
    """Уменьшает параметры Сила, Выносливость, Ловкость на 4"""

    def __init__(self, base):
        self.base = base

        self.stats = self.base.get_stats()

        self.stats["Strength"] -= 4
        self.stats["Endurance"] -= 4
        self.stats["Agility"] -= 4

        self.negative_effects = self.base.get_negative_effects()

        self.negative_effects.append(__class__.__name__)

    def get_stats(self):
        return self.base.get_stats()

    def get_negative_effects(self):
        return self.negative_effects


class EvilEye(AbstractNegative):
    """Уменьшает параметр Удача на 10"""

    def __init__(self, base):
        self.base = base

        self.stats = self.base.get_stats()

        self.stats["Luck"] -= 10

        self.negative_effects = self.base.get_negative_effects()

        self.negative_effects.append(__class__.__name__)

    def get_stats(self):
        return self.base.get_stats()

    def get_negative_effects(self):
        return self.negative_effects


class Curse(AbstractNegative):
    """Уменьшает все основные характеристики на 2"""

    def __init__(self, base):
        self.base = base

        self.stats = self.base.get_stats()

        self.stats["HP"] -= 2
        self.stats["MP"] -= 2
        self.stats["SP"] -= 2
        self.stats["Strength"] -= 2
        self.stats["Perception"] -= 2
        self.stats["Endurance"] -= 2
        self.stats["Charisma"] -= 2
        self.stats["Intelligence"] -= 2
        self.stats["Agility"] -= 2
        self.stats["Luck"] -= 2

        self.negative_effects = self.base.get_negative_effects()

        self.negative_effects.append(__class__.__name__)

    def get_stats(self):
        return self.base.get_stats()

    def get_negative_effects(self):
        return self.negative_effects


my_hero = Hero()

my_hero = Blessing(my_hero)
my_hero = Blessing(my_hero)
my_hero = Weakness(my_hero)
my_hero = Curse(my_hero)
my_hero = Bersek(my_hero.base.base)
my_hero = EvilEye(my_hero.base.base)

print(my_hero.get_stats())
print(my_hero.get_positive_effects())
print(my_hero.get_negative_effects())
