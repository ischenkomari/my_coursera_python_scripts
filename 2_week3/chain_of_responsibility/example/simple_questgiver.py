from simple_event import NullHandler
from simple_quests import *


class QuestGiver:

    def __init__(self):
        self.handlers = QuestCarry(QuestHunt(QuestSpeak(NullHandler())))
        self.events = []

    def add_quest(self, event):
        self.events.append(event)

    def handle_quests(self, character):
        for event in self.events:
            self.handlers.handle(character, event)
