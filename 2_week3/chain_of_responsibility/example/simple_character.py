class Character:
    def __init__(self):
        self.name = "SomeName"
        self.xp = 0
        self.passed_quests = set()
        self.taken_quests = set()

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name
