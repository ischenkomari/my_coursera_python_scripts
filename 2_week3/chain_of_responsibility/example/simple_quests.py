from simple_event import NullHandler

QUEST_SPEAK, QUEST_HUNT, QUEST_CARRY = "QSPEAK", "QHUNT", "QCARRY"


class QuestSpeak(NullHandler):
    def handle(self, char, event):
        if event.kind == QUEST_SPEAK:
            quest_name = "Speak with farmer"
            xp = 100
            if quest_name not in (char.passed_quests | char.taken_quests):
                print(f"Quest is taken: \"{quest_name}\"")
                char.taken_quests.add(quest_name)
            elif quest_name in char.taken_quests:
                print(f"Quest is done: \"{quest_name}\"")
                char.passed_quests.add(quest_name)
                char.taken_quests.remove(quest_name)
                char.xp += xp
        else:
            print("Pass quest forward")
            super().handle(char, event)


class QuestHunt(NullHandler):
    def handle(self, char, event):
        if event.kind == QUEST_HUNT:
            quest_name = "Rat hunting"
            xp = 300
            if quest_name not in (char.passed_quests | char.taken_quests):
                print(f"Quest is taken: \"{quest_name}\"")
                char.taken_quests.add(quest_name)
            elif quest_name in char.taken_quests:
                print(f"Quest is done: \"{quest_name}\"")
                char.passed_quests.add(quest_name)
                char.taken_quests.remove(quest_name)
                char.xp += xp
        else:
            print("Pass quest forward")
            super().handle(char, event)


class QuestCarry(NullHandler):
    def handle(self, char, event):
        if event.kind == QUEST_CARRY:
            quest_name = "Boards carrying"
            xp = 200
            if quest_name not in (char.passed_quests | char.taken_quests):
                print(f"Quest is taken: \"{quest_name}\"")
                char.taken_quests.add(quest_name)
            elif quest_name in char.taken_quests:
                print(f"Quest is done: \"{quest_name}\"")
                char.passed_quests.add(quest_name)
                char.taken_quests.remove(quest_name)
                char.xp += xp
        else:
            print("Pass quest forward")
            super().handle(char, event)
