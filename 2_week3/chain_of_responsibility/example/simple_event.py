class Event:
    def __init__(self, kind):
        self.kind = kind

    def __repr__(self):
        return self.kind

    def __str__(self):
        return self.kind


class NullHandler:
    def __init__(self, successor=None):
        self.__successor = successor

    def handle(self, char, event):
        if self.__successor is not None:
            self.__successor.handle(char, event)
