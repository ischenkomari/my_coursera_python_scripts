from simple_character import Character
from simple_event import *
from simple_questgiver import QuestGiver
from simple_quests import *

all_quest = [Event(QUEST_CARRY), Event(QUEST_HUNT), Event(QUEST_SPEAK)]

quest_giver = QuestGiver()

for quest in all_quest:
    quest_giver.add_quest(quest)

player = Character()

quest_giver.handle_quests(player)
player.taken_quests = {'Speak with farmer', 'Boards carrying'}
quest_giver.handle_quests(player)
