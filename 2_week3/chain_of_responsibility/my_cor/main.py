from chain import Chain
from events import EventGet, EventSet


class SomeObject:
    def __init__(self):
        self.integer_field = 0
        self.float_field = 0.0
        self.string_field = ""


obj = SomeObject()
chain = Chain()

# вернуть значение obj.integer_field
chain.handle(obj, EventGet(int))

# вернуть значение obj.string_field
chain.handle(obj, EventGet(str))

# вернуть значение obj.float_field
chain.handle(obj, EventGet(float))

# установить значение obj.integer_field =1
chain.handle(obj, EventSet(1))

# установить значение obj.float_field = 1.1
chain.handle(obj, EventSet(1.1))

# установить значение obj.string_field = "str"
chain.handle(obj, EventSet("str"))
