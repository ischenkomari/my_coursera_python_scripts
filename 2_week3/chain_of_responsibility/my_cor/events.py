import constants


class EventGet:
    def __init__(self, event_type):
        self.kind = constants.GET
        self.type = event_type


class EventSet:
    def __init__(self, event_value):
        self.kind = constants.SET
        self.value = event_value
        self.type = type(event_value)
