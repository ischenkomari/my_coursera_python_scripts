import constants


class NullHandler:
    def __init__(self, successor=None):
        self.__successor = successor

    def handle(self, obj, event):
        if self.__successor is not None:
            self.__successor.handle(obj, event)


class IntHandler(NullHandler):
    def handle(self, obj, event):
        if event.type is int:
            if event.kind == constants.GET:
                print("Return int field:", obj.integer_field)
                return obj.integer_field
            elif event.kind == constants.SET:
                obj.integer_field = event.value
                print("Set new int field: ", obj.integer_field)
        else:
            print("Pass quest forward")
            super().handle(obj, event)


class FloatHandler(NullHandler):
    def handle(self, obj, event):
        if event.type is float:
            if event.kind == constants.GET:
                print("Return float field:", obj.float_field)
                return obj.float_field
            elif event.kind == constants.SET:
                obj.float_field = event.value
                print("Set new float: ", obj.float_field)
        else:
            print("Pass quest forward")
            super().handle(obj, event)


class StrHandler(NullHandler):
    def handle(self, obj, event):
        if event.type is str:
            if event.kind == constants.GET:
                print("Return string field:", obj.string_field)
                return obj.string_field
            elif event.kind == constants.SET:
                obj.string_field = event.value
                print("Set new string field: ", obj.string_field)
        else:
            print("Pass quest forward")
            super().handle(obj, event)


class Chain:
    def __init__(self):
        self.handlers = IntHandler(FloatHandler(StrHandler(NullHandler())))

    def handle(self, obj, event):
        self.handlers.handle(obj, event)
