from abc import abstractmethod


class ObservableEngine():
    def __init__(self):
        self.__subscribers = set()

    def subscribe(self, subscriber):
        self.__subscribers.add(subscriber)

    def unsubscribe(self):
        self.__subscribers.remove(subscriber)

    def notify(self, message):
        for subscriber in self.__subscribers:
            subscriber.update(message)


class AbstractObserver():
    @abstractmethod
    def update(self):
        pass


class ShortNotificationPrinter(AbstractObserver):
    def __init__(self):
        self.achievements = set()

    def update(self, message):
        self.achievements.add(message["title"])


class FullNotificationPrinter(AbstractObserver):
    def __init__(self):
        self.achievements = list()

    def update(self, message):
        if message not in self.achievements:
            self.achievements.append(message)


full_notification_printer = FullNotificationPrinter()
short_notification_printer = ShortNotificationPrinter()

engine = ObservableEngine()
engine.subscribe(full_notification_printer)
engine.subscribe(short_notification_printer)
message1 = {"title": "Покоритель",
            "text": "Дается при выполнении всех заданий в игре"}
message2 = {"title": " Капитан",
            "text": "Дается при выполнении задания море"}
message3 = {"title": "Покоритель",
            "text": "Дается при выполнении всех заданий в игре"}

engine.notify(message1)
engine.notify(message2)
engine.notify(message3)

print(short_notification_printer.achievements)
print(full_notification_printer.achievements)
