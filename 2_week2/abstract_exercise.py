from abc import ABC, abstractmethod


class A(ABC):
    @abstractmethod
    def do_something(self):
        print("Hi!")

class B(A):

    def do_something(self):
        print("Hi from B")

    def do_something_else(self):
        print("Hello")

b=B()
b.do_something()
b.do_something_else()